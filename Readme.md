# Instructions

## Mask-RCNN train
Import '**MaskRCNN_colab_train.ipynb**' into your Google Colab system and run it

## Mask-RCNN inference
Import '**MaskRCNN_colab_inference.ipynb**' into your Google Colab system and run it
